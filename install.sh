#!/bin/bash
# RUN WITH: source <(curl -s https://gitlab.com/jrapp1/sv-infra2/-/raw/master/install.sh)
wget https://repo.zabbix.com/zabbix/5.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.2-1+ubuntu20.04_all.deb
dpkg -i zabbix-release_5.2-1+ubuntu20.04_all.deb
rm zabbix-release_5.2-1+ubuntu20.04_all.deb
apt update
apt install zabbix-agent -y
mv /etc/zabbix/zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf.dist
echo "b24366ee440a0c9101eedfb625003b888f55afeec15d02eba62863eaffb1757e74ceab28497c96dcba49bdea0709cfd3c8333643bb8d550f728c6251d4c5cc42" > /etc/zabbix/agentd.psk
cat << EOF > /etc/zabbix/zabbix_agentd.conf
HostnameItem=system.run[hostname -f]
HostInterfaceItem=system.run[hostname -f]
HostMetadataItem=system.uname
PidFile=/run/zabbix/zabbix_agentd.pid
LogFile=/var/log/zabbix/zabbix_agentd.log
LogFileSize=10
Server=zabbix.infra.shopvia.org
ServerActive=zabbix.infra.shopvia.org
Include=/etc/zabbix/zabbix_agentd.d/*.conf
TLSConnect=psk
TLSAccept=psk
TLSPSKFile=/etc/zabbix/agentd.psk
TLSPSKIdentity=shopvia
EOF

mkdir /var/lib/zabbix
chown -R zabbix:zabbix /var/lib/zabbix

source <(curl -s https://hyperchicken.m2.spreetail.org/jrapp/sv-infra/-/raw/master/zbx-scripts/systemd/install.sh)

systemctl enable zabbix-agent
systemctl restart zabbix-agent
