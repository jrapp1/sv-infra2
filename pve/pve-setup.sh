#!/bin/bash
# RUN WITH: source <(curl -s https://hyperchicken.m2.spreetail.org/jrapp/sv-infra/-/raw/master/pve/pve-setup.sh)

rm /etc/apt/sources.list.d/pve-enterprise.list
echo "deb http://download.proxmox.com/debian buster pve-no-subscription" > /etc/apt/sources.list.d/pve.list

apt update
apt -y install ifupdown2
apt -y upgrade

source <(curl -s https://hyperchicken.m2.spreetail.org/jrapp/sv-infra/-/raw/master/install-debian.sh)
