#!/usr/bin/env bash

# Configurable items
NOMAD_TOKEN="INSERT-NOMAD-TOKEN"
PGBOUNCER_DATABASE_INI="/etc/pgbouncer/pgbouncer.database.ini"
REPMGR_DB="repmgr"
REPMGR_USER="postgres"
NOMAD_TEMPLATE_SOURCE="http://consul.net.infra.shopvia.org:8501/v1/kv/pgbouncer.nomad?raw"
PGBOUNCER_DATABASES=`psql -d postgres -U ${REPMGR_USER} -t -A -c "select datname from pg_database where datname not in ('postgres', 'template1', 'template0', 'repmgr')"`

d=`date +%m-%d-%Y-%H-%M-%S`
PGBOUNCER_DATABASE_INI_NEW="/tmp/pgbouncer.database.ini.${d}"

touch $PGBOUNCER_DATABASE_INI_NEW

for PGBOUNCER_DATABASE in $PGBOUNCER_DATABASES
do

    echo "Parsing  ${PGBOUNCER_DATABASE}"
    psql -d $REPMGR_DB -U $REPMGR_USER -t -A \
              -c "SELECT '${PGBOUNCER_DATABASE}-rw= host='|| node_name ||' dbname=${PGBOUNCER_DATABASE}' \
                  FROM repmgr.nodes \
                  WHERE active = TRUE AND type='primary' ORDER BY priority ASC" >> $PGBOUNCER_DATABASE_INI_NEW

    psql -d $REPMGR_DB -U $REPMGR_USER -t -A \
              -c "SELECT '${PGBOUNCER_DATABASE}-ro= host='|| node_name ||' dbname=${PGBOUNCER_DATABASE}' \
                  FROM repmgr.nodes \
                  WHERE active = TRUE AND type='standby' ORDER BY priority ASC" >> $PGBOUNCER_DATABASE_INI_NEW

done

PGB_DATABASES=`cat ${PGBOUNCER_DATABASE_INI_NEW} | paste -sd "," -`


rm $PGBOUNCER_DATABASE_INI_NEW

echo "Downloading latest Nomad template"
curl --silent $NOMAD_TEMPLATE_SOURCE > /tmp/job.template

sed "s|%DATABASES|$PGB_DATABASES|g" /tmp/job.template > /tmp/job.nomad

rm /tmp/job.template

echo "Updating Nomad Job"
curl --silent --header "X-Nomad-Token: ${NOMAD_TOKEN}" --request POST --data @/tmp/job.nomad http://nomad.nmd.infra.shopvia.org/v1/job/pgbouncer >> /tmp/nomad.log

rm /tmp/job.nomad

echo "Reconfiguration of pgbouncer complete"
