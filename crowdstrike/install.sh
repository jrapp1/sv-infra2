#!/bin/bash
# RUN WITH: sudo bash -c "source <(curl -s https://hyperchicken.m2.spreetail.org/jrapp/sv-infra/-/raw/master/crowdstrike/install.sh)"
wget https://hyperchicken.m2.spreetail.org/jrapp/sv-infra/-/raw/master/crowdstrike/falcon-sensor_5.43.0-10805_amd64.deb
apt install libnl-genl-3-200 -y
dpkg -i falcon-sensor_5.43.0-10805_amd64.deb
/opt/CrowdStrike/falconctl -s --cid=3CBE6D375E1D4276974265F541F77E2A-1C
systemctl start falcon-sensor
