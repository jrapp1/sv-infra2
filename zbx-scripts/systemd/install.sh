#!/bin/bash
# RUN WITH: # source <(curl -s https://gitlab.com/jrapp1/sv-infra2/-/raw/master/zbx-scripts/systemd/install.sh)

mkdir -p /var/lib/zabbix
chown zabbix:zabbix /var/lib/zabbix

curl https://gitlab.com/jrapp1/sv-infra2/-/raw/master/zbx-scripts/systemd/zbx_service_discovery.sh > /var/lib/zabbix/zbx_service_discovery.sh
curl https://gitlab.com/jrapp1/sv-infra2/-/raw/master/zbx-scripts/systemd/zbx_service_restart_check.sh > /var/lib/zabbix/zbx_service_restart_check.sh
curl https://gitlab.com/jrapp1/sv-infra2/-/raw/master/zbx-scripts/systemd/userparameter_systemd_services.conf > /etc/zabbix/zabbix_agentd.d/userparameter_systemd_services.conf

chown zabbix:zabbix /var/lib/zabbix/zbx_service_discovery.sh
chown zabbix:zabbix /var/lib/zabbix/zbx_service_restart_check.sh

chmod 770 /var/lib/zabbix/zbx_service_discovery.sh
chmod 770 /var/lib/zabbix/zbx_service_restart_check.sh

echo "getty|autovt" > /etc/zabbix/service_discovery_blacklist
echo "sshd|zabbix-agent|elasticsearch" > /etc/zabbix/service_discovery_whitelist

systemctl restart zabbix-agent.service
